import React from 'react'
import packageJson from '../../package.json'

const JsPackages = () => {
  return (
    <div>
      <div className="italic font-bold">NPM</div>
      {packageJson.devDependencies && (
        <div>
          <div className="underline my-3">Development Packages</div>
          <ul className="ml-3">
            {Object.keys(packageJson.devDependencies).map((dev) => (
              <li
                key={dev}
              >{`${dev} : ${packageJson.devDependencies[dev]}`}</li>
            ))}
          </ul>
        </div>
      )}
      <div>
        <div className="underline my-3">Production Packages</div>
        <ul className="ml-3">
          {Object.keys(packageJson.dependencies).map((p) => (
            <li key={p}>{`${p} : ${packageJson.dependencies[p]}`}</li>
          ))}
        </ul>
      </div>
    </div>
  )
}

const readme = (props) => {
  return (
    <>
      <div className="text-lg font-bold text-center">Readme</div>
      <div className="italic font-bold text-center">Installed Packages</div>
      <div className="flex flex-row justify-between px-10">
        <JsPackages />
      </div>
    </>
  )
}

export default readme
