import React from 'react'
import Card from 'components/UI/Card'
import EaseIn from 'components/animations/EaseIn'
import { Link } from '@reach/router'
import packageJson from '../../package.json'

const CardsDisplay = () => (
  <Card title="React - Starter Pack V2">
    <div className="italic">A simple starter pack for using React.js</div>
  </Card>
)

const GettingStarted = () => {
  const { dependencies } = packageJson

  return (
    <Card title="Getting Started">
      <div className="font-mono">
        <div>
          Go to{' '}
          <Link className="font-bold underline" to="/readme">
            Readme
          </Link>{' '}
          for installed npm packages
        </div>

        <div className="italic font-mono mt-5">Specifications :</div>
        <div> React.js {dependencies.react}</div>
      </div>
    </Card>
  )
}

const index = (props) => {
  return (
    <div className="gap-5 flex flex-col justify-center">
      <EaseIn>
        <CardsDisplay />
        <GettingStarted />
      </EaseIn>
    </div>
  )
}

export default index
