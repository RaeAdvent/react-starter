import React from 'react'
import { Link } from '@reach/router'

const Menu = ({ to, children, ...props }) => (
  <Link className="p-2 rounded-sm hover:bg-gray-100" to={to}>
    {children}
  </Link>
)

export default function Navigation() {
  return (
    <div className="w-screen border bg-white">
      <div className="mx-5 my-2 flex flex-row space-x-5">
        <Menu to="/">Home</Menu>
        <Menu to="/readme">Readme</Menu>
      </div>
    </div>
  )
}
