import React from 'react'
import { motion } from 'framer-motion'
import { Location } from '@reach/router'

export default function PageTransition({ children, route }) {
  return (
    <Location>
      {props => {
        return (
          <motion.div
            key={props.location.pathname}
            initial="pageInitial"
            animate="pageAnimate"
            variants={{
              pageInitial: {
                opacity: 0,
              },
              pageAnimate: {
                opacity: 1,
              },
            }}
          >
            {children}
          </motion.div>
        )
      }}
    </Location>
  )
}
