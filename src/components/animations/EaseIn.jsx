import React from 'react'
import { motion } from 'framer-motion'

export default function EaseIn({ children }) {
  const initialState = { y: '300px', opacity: 0 }

  const animations = { y: 0, opacity: 1 }

  const transitionSettings = { ease: 'easeIn', duration: 0.5 }
  const list = {
    visible: { opacity: 1 },
    hidden: { opacity: 0 },
  }

  return (
    <motion.div
      initial={initialState}
      animate={animations}
      transition={transitionSettings}
      variants={list}
    >
      {children}
    </motion.div>
  )
}
